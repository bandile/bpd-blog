# BPD BLOG - Backend


This is a Laravel project built with Composer
<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## First Step:
Create a **.env** file from the **.env.example** file.
- Check the `SANCTUM_STATEFUL_DOMAINS` variable, change the ports to the relevant ones where the frontend and backend will be hosted.
- Check all the `DB_HOST`, `DB_USERNAME`, `DB_PASSWORD` variables and change to the relevant information for your local database.
- Create a database called **bpd-blog**

## Available Scripts

In the project directory, you can run:

### `composer install`
This will install all the packages required to run the project.

### `php artisan migrate`
This will create all the tables in the database needed for this project

### `php artisan db:seed`
This will add some dummy data to the database

### `php artisan serve`

Runs the backend on  [http://localhost:8000](http://localhost:8000) or on the next available port

## Login information
A default user will be created with the login information bellow (you can also register a new user from the frontend)

###User
Email: user@gmail.com
Password: user12345

