<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Webpatser\Uuid\Uuid;

class BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('blog_posts')->insert([
            'uuid' => (string)Uuid::generate(4),
            'title' => 'Blog Post 1',
            'description' => 'Felis adipiscing leo adipiscing erat magna vivamus ex phasellus quis orci vel molestie suspendisse vel urna fusce eget phasellus nec maximus enim lacus phasellus leo.
                            Diam orci urna sem eu id facilisis congue accumsan gravida tincidunt massa tristique nulla ut proin dolor morbi tristique morbi nisl maecenas molestie tristique morbi.
                            Scelerisque diam nisl vel sem maximus metus commodo id magna ex quisque id arcu mi sit vivamus mi bibendum maximus eu magna nec elementum bibendum.
                            Ac magna diam gravida congue nunc sollicitudin lorem accumsan scelerisque proin sem tortor condimentum fusce leo nec metus bibendum commodo portaest ac elementum bibendum vivamus.',
            'author' => 'Bandile Dlamini',
            'created_at' => Carbon::now()
        ]);

        DB::table('blog_posts')->insert([
            'uuid' => (string)Uuid::generate(4),
            'title' => 'Blog Post 2',
            'description' => 'Interdum massa quisque vel pellentesque et sem quis varius id sollicitudin elementum vivamus placerat vel ipsum urna tristique orci fusce ut dolor tempus sem sed.
                            Arcu lacus lacus felis sit vel accumsan amet molestie arcu nulla erat molestie pellentesque metus sed pellentesque orci ex ut sit urna a placerat interdum.
                            Metus mi portaest varius leo erat adipiscing nec diam quam euismod fusce eu eu nunc sed adipiscing urna accumsan amet tempus congue elementum suspendisse phasellus.
                            Nec elit quis tempus accumsan eget eget tristique vivamus phasellus erat portaest cursus vel varius massa ipsum molestie eget a nisi et tortor tortor quisque.',
            'author' => 'Bandile Dlamini',
            'created_at' => Carbon::now()
        ]);

        DB::table('blog_posts')->insert([
            'uuid' => (string)Uuid::generate(4),
            'title' => 'Blog Post 3',
            'description' => 'Tincidunt euismod scelerisque facilisis orci maecenas bibendum quisque magna elit a sed massa accumsan sollicitudin maecenas magna fusce felis pellentesque ex scelerisque condimentum varius maximus.
                            Eget quam varius amet maximus accumsan arcu metus molestie felis lacus aliquam pellentesque placerat tortor purus enim nunc ut ac quam ipsum ipsum eu purus.
                            Orci euismod nisi morbi interdum felis sit tristique nisl id sem placerat consectetur vivamus eget pellentesque phasellus ipsum quisque aliquam nisi ex massa ipsum nec.
                            Leo mi facilisis varius purus a accumsan eget commodo bibendum tristique phasellus a felis maximus a commodo scelerisque sollicitudin tortor id interdum tincidunt erat vel.',
            'author' => 'Bandile Dlamini',
            'created_at' => Carbon::now()
        ]);
    }
}
