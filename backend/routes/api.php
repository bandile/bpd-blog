<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', 'App\Http\Controllers\Auth\LoginController@login');
Route::post('/users', 'App\Http\Controllers\UserController@store')->name('users.store');
Route::get('/blog-posts', 'App\Http\Controllers\BlogController@index')->name('blog-posts');
Route::get('/blog-posts-all', 'App\Http\Controllers\BlogController@getAll')->name('blog-posts.all');

Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::post('/logout', 'App\Http\Controllers\Auth\LoginController@logout')->name('logout');

    Route::get('/users', 'App\Http\Controllers\UserController@index')->name('users.get');
    Route::get('/users/{user}', 'App\Http\Controllers\UserController@show')->name('users.show');
    Route::patch('/users/{user}', 'App\Http\Controllers\UserController@update')->name('users.update');
    Route::delete('/users/{user}', 'App\Http\Controllers\UserController@destroy')->name('users.destroy');

    Route::post('/blog-posts', 'App\Http\Controllers\BlogController@store')->name('blog-posts.store');
    Route::get('/blog-posts/{blogpost}', 'App\Http\Controllers\BlogController@show')->name('blog-posts.show');
    Route::patch('/blog-posts/{blogpost}', 'App\Http\Controllers\BlogController@update')->name('blog-posts.update');
    Route::delete('/blog-posts/{blogpost}', 'App\Http\Controllers\BlogController@destroy')->name('blog-posts.destroy');

});
