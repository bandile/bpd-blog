<?php

namespace App\Http\Controllers;

use App\Http\Requests\BlogRequest;
use App\Services\BlogService;

class BlogController extends Controller
{
    protected $blogService;

	public function __construct(BlogService $blogService)
	{
		$this->blogService = $blogService;
	}

    /**
     * Display a listing of the resource.
     *
     * @return IlluminateHttpResponse
     */
    public function index()
    {
        $blogs = $this->blogService->getBlogs();
        return response()->json($blogs);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  BlogRequest  $request
     * @return IlluminateHttpResponse
     */
    public function store(BlogRequest $request)
    {
        $blog = $this->blogService->store($request);
        return response()->json([
            'message'=> 'Blog created',
            'Blog' => $blog,
            'data' => $this->blogService->getBlogs()]);
    }
    /**
     * Display the specified resource.
     *
     * @param  AppBlog  $blog
     * @return IlluminateHttpResponse
     */
    public function show($uuid = null)
    {
        return $this->blogService->getBlog($uuid);
    }
    public function get($id = null)
    {
        return $this->blogService->getBlogById($id);
    }
    public function getAll()
    {
        $blogs = $this->blogService->getBlogsAll();
        return response()->json($blogs);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  AppBlog  $blog
     * @return IlluminateHttpResponse
     */
    /**
     * Update the specified resource in storage.
     *
     * @param  BlogRequest  $request
     * @param  AppBlog  $blog
     * @return IlluminateHttpResponse
     */
    public function update(BlogRequest $request, $uuid = null)
    {
        $this->blogService->update($request, $uuid);

        return response()->json([
            'message' => 'Blog updated!',
            'Blog' => $this->blogService->getBlog($uuid),
            'data' => $this->blogService->getBlogs()
        ]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  AppBlog  $blog
     * @return IlluminateHttpResponse
     */
    public function destroy($uuid = null)
    {
        $this->blogService->delete($uuid);
        return response()->json([
            'message' => 'Blog deleted',
            'data' => $this->blogService->getBlogs()
        ]);

    }
}
