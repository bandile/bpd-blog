<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Services\UserService;

class UserController extends Controller
{
    protected $userService;

	public function __construct(UserService $userService)
	{
		$this->userService = $userService;
        // $this->middleware('auth');
        // $this->middleware('role:ROLE_USER');
	}

    /**
     * Display a listing of the resource.
     *
     * @return IlluminateHttpResponse
     */
    public function index()
    {
        $users = $this->userService->getUsers();
        return response()->json($users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  UserRequest  $request
     * @return IlluminateHttpResponse
     */
    public function store(UserRequest $request)
    {
        $user = $this->userService->store($request);

        $token = $user->createToken('user-token')->plainTextToken;

        $response = [
            'message'=> 'User created',
            'token' => $token,
            'user' => $user,
            'data' => $this->userService->getUsers()];

        return response($response, 201);
    }
    /**
     * Display the specified resource.
     *
     * @param  AppUser  $user
     * @return IlluminateHttpResponse
     */
    public function show($uuid = null)
    {
        return $this->userService->getUser($uuid);
    }
    public function get($id = null)
    {
        return $this->userService->getUserById($id);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  AppUser  $user
     * @return IlluminateHttpResponse
     */
    /**
     * Update the specified resource in storage.
     *
     * @param  UserUpdateRequest  $request
     * @param  AppUser  $user
     * @return IlluminateHttpResponse
     */
    public function update(UserUpdateRequest $request, $uuid = null)
    {
        $this->userService->update($request, $uuid);

        return response()->json([
            'message' => 'User updated!',
            'User' => $this->userService->getUser($uuid),
            'data' => $this->userService->getUsers()
        ]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  AppUser  $user
     * @return IlluminateHttpResponse
     */
    public function destroy($uuid = null)
    {
        $this->userService->delete($uuid);
        return response()->json([
            'message' => 'User deleted',
            'data' => $this->userService->getUsers()
        ]);

    }
}
