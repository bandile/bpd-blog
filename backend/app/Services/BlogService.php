<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\BlogRequest;
use App\Models\Blog;

class BlogService extends Controller
{

    public function getBlogsAll(){
        $blogs = Blog::orderBy('created_at', 'desc')->get();
        return $blogs;
    }

    public function getBlogs(){
        $blogs = Blog::orderBy('created_at', 'desc')->paginate(10);
        return $blogs;
    }

    public function getBlog($uuid = null)
    {
        return Blog::where('uuid', $uuid)->firstOrFail();
    }
    public function getBlogsById($id = null)
    {
        return Blog::where('id', $id)->firstOrFail();
    }

    public function commonElements(Request $request,$entry)
    {
        $entry->title = $request->input('title');
        $entry->description = $request->input('description');
        $entry->author = $request->input('author');
        $entry->save();

        return $entry;
    }

    public function store(BlogRequest $request)
    {
        $entry = new Blog();
        return $this->commonElements($request,$entry);
    }

    public function update(BlogRequest $request, $uuid = null)
    {
        $entry = Blog::where('uuid',$uuid)->firstOrFail();
        $this->commonElements($request, $entry);
        $entry->save();
        return $entry;
    }

    public function delete($uuid = null)
    {
        $entry = Blog::where('uuid', $uuid)->firstOrFail();
        $entry->delete();
    }
}
