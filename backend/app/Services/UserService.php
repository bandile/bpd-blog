<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Models\User;

class UserService extends Controller
{

    public function getUsers(){
        return User::all();
    }

    public function getUser($uuid = null)
    {
        return User::where('uuid', $uuid)->firstOrFail();
    }
    public function getUserById($id = null)
    {
        return User::where('id', $id)->firstOrFail();
    }

    public function commonElements(Request $request,$entry)
    {
        $entry->name = $request->input('name');
        $entry->email = $request->input('email');
        $entry->email_verified_at = $request->input('email_verified_at');
        $entry->password = bcrypt($request->input('password'));
        $entry->save();

        return $entry;
    }

    public function commonElementsUpdate(Request $request,$entry)
    {
        $entry->name = $request->input('name');
        $entry->email = $request->input('email');
        $entry->email_verified_at = $request->input('email_verified_at');
        $entry->save();

        return $entry;
    }

    public function store(UserRequest $request)
    {
        $entry = new User();
        return $this->commonElements($request,$entry);
    }

    public function update(UserUpdateRequest $request, $uuid = null)
    {
        $entry = User::where('uuid',$uuid)->firstOrFail();
        $this->commonElementsUpdate($request, $entry);
        $entry->save();
        return $entry;
    }

    public function delete($uuid = null)
    {
        $entry = User::where('uuid', $uuid)->firstOrFail();
        $entry->delete();
    }
}
