<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Models\User;

class AuthService extends Controller
{

    public function getUsers(){
        return User::all();
    }

    public function getUser($uuid = null)
    {
        return User::where('uuid', $uuid)->firstOrFail();
    }

    public function getUserById($id = null)
    {
        return User::where('id', $id)->firstOrFail();
    }

    public function commonElements(Request $request,$entry)
    {
        $entry->email = $request->input('email');
        $entry->password = $request->input('password');
        $entry->save();

        return $entry;
    }

    public function store(UserRequest $request)
    {
        $entry = new User();
        return $this->commonElements($request,$entry);
    }

    public function update(UserRequest $request, $uuid = null)
    {
        $entry = User::where('uuid',$uuid)->firstOrFail();
        $this->commonElements($request, $entry);
        $entry->save();
        return $entry;
    }

    public function delete($uuid = null)
    {
        $entry = User::where('uuid', $uuid)->firstOrFail();
        $entry->delete();
    }
}
