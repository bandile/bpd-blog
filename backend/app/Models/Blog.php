<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Webpatser\Uuid\Uuid;

class Blog extends Model
{
    use HasFactory;
    use SoftDeletes;

    public $table = "blog_posts";

    public static function boot()
    {
        parent::boot();
        self::creating(
            function ($model)
            {
                if (!$model->uuid)
                {
                    $model->uuid = (string)Uuid::generate(4);
                }
            }
        );
    }

    public function getRouteKeyName()
    {
        return 'uuid';
    }
}
