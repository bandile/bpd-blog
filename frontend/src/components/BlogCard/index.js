import React, {useState} from 'react';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import moment from 'moment';
import Dialog from '../Dialog';
import TextTruncate from 'react-text-truncate';

export default function BlogCard(props) {
  const [elivation, setElivation] = useState(1);
  const [open, setOpen] = React.useState(false);

  return (
    <Card 
        sx={{ maxWidth: 345 }} 
        elevation={elivation} 
        onMouseOver={() => setElivation(5)} 
        onMouseLeave={() => setElivation(1)}
        style={{cursor: 'pointer'}}
    >
      <CardHeader
        title={props.blog.title}
        subheader={props.blog.type}
        onClick={() => setOpen(true)}
      />
      <CardContent onClick={() => setOpen(true)}>
        <TextTruncate
            line={4}
            element="span"
            truncateText="…"
            text={props.blog.description}
            textTruncateChild={<span style={{color: 'blue', textDecoration: 'underline'}}>Read more</span>}
        />
        <Typography variant="body2" color="text.secondary" style={{ textAlign: 'right', marginTop: '20px' }}>
          {`${props.blog.author} - ${moment(props.blog.created_at).format("D MMM yyyy")}`}
        </Typography>
      </CardContent>
      <Dialog 
        open={open} 
        setOpen={setOpen}
        blog={props.blog}
        />
    </Card>
  );
}
