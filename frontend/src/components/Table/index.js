import React, {forwardRef, useState, useEffect} from "react";
import MaterialTable from "material-table";


import AddBox from '@mui/icons-material/AddBox';
import ArrowDownward from '@mui/icons-material/ArrowDownward';
import Check from '@mui/icons-material/Check';
import ChevronLeft from '@mui/icons-material/ChevronLeft';
import ChevronRight from '@mui/icons-material/ChevronRight';
import Clear from '@mui/icons-material/Clear';
import DeleteOutline from '@mui/icons-material/DeleteOutline';
import Edit from '@mui/icons-material/Edit';
import FilterList from '@mui/icons-material/FilterList';
import FirstPage from '@mui/icons-material/FirstPage';
import LastPage from '@mui/icons-material/LastPage';
import Remove from '@mui/icons-material/Remove';
import SaveAlt from '@mui/icons-material/SaveAlt';
import Search from '@mui/icons-material/Search';
import ViewColumn from '@mui/icons-material/ViewColumn';
import apiClient from "../../apiClient";

const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
    DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
  };

export default function Table(props){
    const [data, setData] = useState([]);

    useEffect(() => {
        apiClient.get(
            props.apiGet ? `/api/${props.apiGet}` : `/api/${props.api}`,
            {
              headers: { Authorization: `Bearer ${localStorage.getItem("Token")}` },
              'Accept': 'application/json',
              'Content-Type': 'application/json',
            })
            .then((res) => {
                // console.log(res.data);
                setData(res.data);
            })
            .catch((e) => {
              console.log(e);
            });
    },[]);

    return(
        <MaterialTable
            style={{marginBottom: '50px'}}
            title={props.title}
            icons={tableIcons}
            options={{
                search: true
            }}
            columns={props.columns}
            data={data}
            editable={{
                onRowAddCancelled: rowData => console.log('Row adding cancelled'),
                onRowUpdateCancelled: rowData => console.log('Row editing cancelled'),
                onRowAdd: newData =>
                    new Promise((resolve, reject) => {
                        apiClient.post(
                            `/api/${props.api}`,
                            {
                                ...newData,
                                author: JSON.parse(localStorage.getItem('User')).name
                            },
                            {
                              headers: { Authorization: `Bearer ${localStorage.getItem("Token")}` },
                              'Accept': 'application/json',
                              'Content-Type': 'application/json',
                            })
                            .then((res) => {
                                setData(res.data);
                            })
                            .catch((e) => {
                              console.log(e);
                            });
                            
                        setTimeout(() => {
                            setData([
                                ...data, 
                                {
                                    ...newData,
                                    created_at: new Date(),
                                    author: JSON.parse(localStorage.getItem('User')).name
                                }
                            ]);

                            resolve();
                        }, 1000);
                    }),
                onRowUpdate: (newData, oldData) =>
                    new Promise((resolve, reject) => {
                        apiClient.patch(
                            `/api/${props.api}/${oldData.uuid}`,
                            {
                                ...newData
                            },
                            {
                              headers: { Authorization: `Bearer ${localStorage.getItem("Token")}` },
                              'Accept': 'application/json',
                              'Content-Type': 'application/json',
                            })
                            .then((res) => {
                                setData(res.data);
                            })
                            .catch((e) => {
                              console.log(e);
                            });

                        setTimeout(() => {
                            const dataUpdate = [...data];
                            const index = oldData.tableData.id;
                            dataUpdate[index] = newData;
                            setData([...dataUpdate]);

                            resolve();
                        }, 1000);
                    }),
                onRowDelete: oldData =>
                    new Promise((resolve, reject) => {

                        apiClient.delete(
                            `/api/${props.api}/${oldData.uuid}`,
                            {
                              headers: { Authorization: `Bearer ${localStorage.getItem("Token")}` },
                              'Accept': 'application/json',
                              'Content-Type': 'application/json',
                            })
                            .then((res) => {
                                setData(res.data);
                            })
                            .catch((e) => {
                              console.log(e);
                            });


                        setTimeout(() => {
                            const dataDelete = [...data];
                            const index = oldData.tableData.id;
                            dataDelete.splice(index, 1);
                            setData([...dataDelete]);

                            resolve();
                        }, 1000);
                    })
            }}
        />
    );
}
