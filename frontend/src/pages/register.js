import React, {useState} from 'react';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import InputAdornment from '@mui/material/InputAdornment';
import { Paper } from '@mui/material';
import LoadingButton from '@mui/lab/LoadingButton';
import Typography from '@mui/material/Typography';
import MailIcon from '@mui/icons-material/Mail';
import PasswordIcon from '@mui/icons-material/Password';
import IconButton from '@mui/material/IconButton';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import { Navigate } from 'react-router-dom';
import apiClient from '../apiClient';
import Snackbar from '@mui/material/Snackbar';
import MuiAlert from '@mui/material/Alert';
import { useFormik } from 'formik';
import * as Yup from 'yup';

const RegisterSchema = Yup.object().shape({
    name: Yup.string()
        .min(3, 'Name must be more than 3 characters')
        .max(40, 'Name must not be more than 40 charaters')
        .required('Required'),
    email: Yup.string().email('Invalid email').required('Required'),
    password: Yup.string()
        .min(8, 'Password must be more than 8 characters')
        .max(60, 'Password must not be more than 60 charaters')
        .required('Required'),
    password_confirmation: Yup.string()
        .min(8, 'Password must be more than 8 characters')
        .max(60, 'Password must not be more than 60 charaters')
        .when("password", {
          is: (value) => (value && value.length > 0 ? true : false),
          then: Yup.string().oneOf(
            [Yup.ref("password")],
            "Both passwords need to be the same"
          ),
        })
        .required("Confirm Password Required"),
});

const Alert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

export default function Register() {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');
  const [redirect, setRedirect] = useState(false);
  const [showPassword, setShowPassword] = useState(false);

  const formik = useFormik({
    initialValues: {
        name: '',
        email: '',
        password: '',
        password_confirmation: ''
    },
    validationSchema: RegisterSchema,
    onSubmit: (values) => {
      setLoading(true);
      handleRegister();
    },
  });

  
  function handleRegister() {
    setLoading(true);

    apiClient.get('/sanctum/csrf-cookie')
      .then(response => {

        apiClient.post('/api/users', 
            formik.values,
            {
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                }
            }
        )
        .then((res) => {
          setError(false);
          setErrorMessage('');
          localStorage.setItem('Token', res.data.token);
          localStorage.setItem('User', JSON.stringify(res.data.user));
          setLoading(false);
          setRedirect(true);
        })
        .catch((e) => {
          console.log("Error" + e);
          setError(true);
          setErrorMessage("Invalid Email/Password");
          setLoading(false);
        });
      })
      .catch((e) => console.log(e));
  }

  const handleClickShowPassword = (e) => {
    e.preventDefault();

    setShowPassword(!showPassword);
  };

  return (
    <Box
      component="div"
      sx={{
        '& .MuiTextField-root': { m: 1, width: '25ch' },
      }}
      noValidate
      autoComplete="off"
      alignItems={"center"}
    >
      {redirect ? <Navigate to='/'/> : null}
      <Paper
        style={{
          width: '30%', 
          minWidth: '400px', 
          minHeight: '400px', 
          margin: '100px auto', 
          padding: '20px', 
          textAlign: 'center',
          backgroundColor: "#f7f7f7"
        }}
        elevation={3}
      >
          <Typography variant="h3" component="div">
            BPD Blog
          </Typography>
          <Typography variant="h5" component="div" gutterBottom>
            Register
          </Typography>

          <form onSubmit={formik.handleSubmit}>
            <TextField
              label="Name"
              id="name"
              name="name"
              value={formik.values.name}
              onChange={formik.handleChange}
              error={formik.touched.name && Boolean(formik.errors.name)}
              helperText={formik.touched.name && formik.errors.name}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <AccountCircleIcon />
                  </InputAdornment>
                ),
              }}
              style={{width: '80%'}}
            />
            <TextField
              label="Email"
              id="email"
              name="email"
              type='email'
              value={formik.values.email}
              onChange={formik.handleChange}
              error={formik.touched.email && Boolean(formik.errors.email)}
              helperText={formik.touched.email && formik.errors.email}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <MailIcon />
                  </InputAdornment>
                ),
              }}
              style={{width: '80%'}}
            />
            <TextField
              label="Password"
              id="password"
              name="password"
              value={formik.values.password}
              type={showPassword ? 'text' : 'password'}
              onChange={formik.handleChange}
              error={formik.touched.password && Boolean(formik.errors.password)}
              helperText={formik.touched.password && formik.errors.password}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <PasswordIcon />
                  </InputAdornment>
                ),
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      edge="end"
                    >
                      {showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
              style={{width: '80%'}}
            />
            <TextField
              label="Confirm Password"
              id="password_confirmation"
              name="password_confirmation"
              value={formik.values.password_confirmation}
              type={showPassword ? 'text' : 'password'}
              onChange={formik.handleChange}
              error={formik.touched.password_confirmation && Boolean(formik.errors.password_confirmation)}
              helperText={formik.touched.password_confirmation && formik.errors.password_confirmation}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <PasswordIcon />
                  </InputAdornment>
                ),
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      edge="end"
                    >
                      {showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
              style={{width: '80%'}}
            />
            <LoadingButton
              loading={loading}
              loadingIndicator="Loading..."
              variant="contained"
              type='submit'
              style={{width: '80%', height: '50px', marginTop: '15px'}}
            >
              Register
            </LoadingButton>
          </form>
      </Paper>
      <Snackbar open={error} autoHideDuration={6000} onClose={() => setError(false)}>
        <Alert onClose={() => setError(false)} severity="error" sx={{ width: '100%' }}>
          {errorMessage}
        </Alert>
      </Snackbar>
    </Box>
  );
}
