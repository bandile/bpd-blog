import moment from "moment";
import React, {useState, useEffect} from "react";
import Navbar from '../components/Navbar';
import Table from '../components/Table';
import apiClient from "../apiClient";

const blogColumns = [
    { title: 'Title', field: 'title' },
    { title: 'Description', field: 'description',},
    { title: 'Date', field: 'created_at', render: rowData => moment(rowData.created_at).format("D MMM yyyy"), editable: 'never'},
    { title: 'Author', field: 'author', editable: 'onUpdate'},
];

export default function Dashboard(){
    const [blogsAll, setBlogsAll] = useState([]);
    const [blogs, setBlogs] = useState([]);

    useEffect(() => {
        apiClient.get(
            `/api/blog-posts-all`,
            {
              headers: { Authorization: `Bearer ${localStorage.getItem("Token")}` },
              'Accept': 'application/json',
              'Content-Type': 'application/json',
            })
            .then((res) => {
                setBlogs(res.data);
                setBlogsAll(res.data);
            })
            .catch((e) => {
              console.log(e);
            });
    },[]);

    return(
        <>
            <Navbar blogs={blogs} setBlogs={setBlogs} blogsAll={blogsAll}/>
            <div
                style={{padding: '50px', backgroundColor: '#f0f0f0'}}
            >
                <Table 
                    title="Blog Posts"
                    columns={blogColumns}
                    api="blog-posts"
                    apiGet="blog-posts-all"
                />
            </div>
        </>
    );
}