import React, {useState, useEffect} from "react";
import Navbar from '../components/Navbar';
import BlogCard from '../components/BlogCard';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Pagination from '@mui/material/Pagination';
import apiClient from "../apiClient";

export default function Home(){
    const [blogs, setBlogs] = useState([]);
    const [blogsAll, setBlogsAll] = useState([]);
    const [loggedIn, setLoggedIn] = useState(false);
    const [page, setPage] = useState(1);
    const [pageCount, setPageCount] = useState(1);
    
    const handleChangePage = (event, value) => {
        setPage(value);
    };

    useEffect(() => {
        if(localStorage.getItem('Token')){
            setLoggedIn(true);
        }

        apiClient.get(
            `/api/blog-posts-all`,
            {
              headers: { Authorization: `Bearer ${localStorage.getItem("Token")}` },
              'Accept': 'application/json',
              'Content-Type': 'application/json',
            })
            .then((res) => {
                setBlogsAll(res.data);
            })
            .catch((e) => {
              console.log(e);
            });
    },[]);

    useEffect(() => {
        apiClient.get(
            `/api/blog-posts?page=${page}`,
            {
              headers: { Authorization: `Bearer ${localStorage.getItem("Token")}` },
              'Accept': 'application/json',
              'Content-Type': 'application/json',
            })
            .then((res) => {
                setBlogs(res.data.data);
                setPageCount(res.data.last_page);
            })
            .catch((e) => {
              console.log(e);
            });
    },[page]);

    return(
        <>
            <Navbar loggedIn={loggedIn} setLoggedIn={setLoggedIn} blogs={blogs} setBlogs={setBlogs} blogsAll={blogsAll}/>
            <div
                style={{padding: '50px'}}
            >
                {blogs ?
                    blogs.length > 0 ?
                        <>
                            <Box sx={{ flexGrow: 1 }} style={{marginTop: '60px'}}>
                                <Grid container item spacing={5} rowSpacing={5} >
                                    {blogs.map((item, index) => {
                                        return <div key={"bpd-blog-post-"+index} style={{width: 'auto', margin: '10px'}}>
                                            <BlogCard 
                                                key={`blog-number-${index}`}
                                                blog={item}
                                                loggedIn={loggedIn}
                                            />
                                        </div>;
                                        })
                                    }
                                </Grid>
                                <Pagination count={pageCount} style={{margin: '0 auto', position: 'absolute', left: '40%'}} page={page} onChange={handleChangePage} />
                            </Box>
                        </>
                        :
                        <h1 style={{textAlign: 'center'}}>No Posts Available</h1>
                :
                    <h1 style={{textAlign: 'center'}}>Something went wrong... Please refresh the page</h1>
                }
            </div>
        </>
    );
}