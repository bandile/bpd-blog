import React from "react";

import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";

import './App.css';
import Register from "./pages/register";
import Login from './pages/login';
import Home from "./pages/home";
import Dashboard from "./pages/dashboard";

function App() {

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />}/>
        <Route path="/login" element={<Login />}/>
        <Route path="/register" element={<Register />}/>
        <Route path="/dashboard" element={<Dashboard />}/>
      </Routes>
    </BrowserRouter>
  );
}

export default App;