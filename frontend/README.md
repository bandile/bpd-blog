# RentMyBike - Frontend

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## First Step:
Open the file ***apiClient.js*** found in **/frontend/src** and change the `baseURL` to the url where the backend is running (if different from http://localhost:8000).

## Available Scripts

In the project directory, you can run:

### `yarn` or `npm install`
This will install all the packages required to run the project.

### `yarn start` or `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

##Basic Usage
This is a blog project, the landing page will allow you to view blog posts whether you are logged in or not. 

The Navbar at the top allows you to **search** for a blog post (search by title of the blog post). The Navbar also has a **Login / Register ** button on the right hand side if you are not logged in. If you are logged in, there will be a user icon button that you can click on and select Logout. The Nav bar will also have a **Dashboard** button which will be visible only to users that are logged in.

The landing page shows each blog post as a card, each card shows the first 4 lines of the post, to see more, click on the card.

To add, update and/or delete blog posts, login and click on the **Dashboard** button in the Navbar. This will take you to a dashboard with a table where you can perform actions on each entry. On the table, there is a plus (+) button n the top right hand corner, click on it to add a new row to the table. Adding a row to the table will automatically create a blog post which will be visible on the landing page.
