# BPD BLOG

The backend for this project is built using **Laravel**
The frontend of this project is built using **React**

This repo contains a `backend` folder and `frontend` folder. 
Follow the steps in the README file found in the respective folders to run the project.
